kernelcolor: kernelcolor.c
	cc -o kernelcolor kernelcolor.c

clean:
	rm kernelcolor

install: kernelcolor
	cp kernelcolor /usr/bin/kernelcolor

uninstall:
	rm /usr/bin/kernelcolor

