#include <stdio.h>

int readcolor (char* s, int n){
	char c;
	int i=0;
	int r=1;
	while (r){
		c = getchar();
		if ((c=='\n') || (c==EOF)){
			r=0;
		} else if ((c != '#' && i < n-1 )) {
			if (c>96) c-=32;
			s[i]=c;
			i++;
		}
	}
	s[n]=0;
	return 0;
}

int main () {
	char col[16][7];
	char colname[3][4] = {
		"red",
		"grn",
		"blu",
	};
	for (int i=0;i<16;i++){
		readcolor(col[i],7);
	}
	for (int i=0;i<3;i++){
		printf ("vt.default_%s=",colname[i]);
		for (int j=0;j<16;j++){
			printf ("0x%c%c,",col[j][2*i],col[j][2*i+1]);
		}
		printf ("\b ");
	}
	return 0;
}
