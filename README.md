# Kernelcolor
A program to generate kernel parameters to apply a specified color scheme to
the TTY.
## Usage:
```sh
kernelcolor < file
```
Files should contain 6-digit hexcodes, one per line, starting with a `#`. The
order should follow the standard numbering 0-15, ie: black, red, green, yellow,
blue, magenta, cyan, white, brblack, brred, brgreen, bryellow, brblue,
brmagenta, brcyan, brwhite. Files are case-insensitive. Any text after the
color code will be ignored. For example:
```
#000000    this comment will be ignored
#aa0000
#00aa00
#aaaa00
#0000aa
#aa00aa
#00aaaa
#aaaaaa
#555555    bright colors start here
#ff5555
#55ff55
#ffff55
#5555ff
#ff55ff
#55ffff
#ffffff
```
